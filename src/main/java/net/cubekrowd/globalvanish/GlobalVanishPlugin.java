/*

    globalvanish
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.globalvanish;

import de.myzelyam.api.vanish.PostPlayerHideEvent;
import de.myzelyam.api.vanish.PostPlayerShowEvent;
import de.myzelyam.api.vanish.VanishAPI;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.pubsub.RedisPubSubAdapter;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.resource.ClientResources;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class GlobalVanishPlugin extends JavaPlugin implements Listener {
    public final Set<UUID> vanishedPlayers = new HashSet<>();
    public RedisClient redisClient;
    public StatefulRedisConnection<String, String> redisConnection;
    public StatefulRedisPubSubConnection<String, String> pubSubConnection;
    public String vanishedSet = "globalvanish:vanished";
    public String notifyChannel = "globalvanish:update";

    public void resetVanishedPlayers() {
        redisConnection.reactive()
                .smembers(vanishedSet)
                .collectList()
                .doOnNext(members -> {
                    synchronized (vanishedPlayers) {
                        vanishedPlayers.clear();
                        for (var member : members) {
                            vanishedPlayers.add(UUID.fromString(member));
                        }
                    }
                })
                .subscribe();
    }

    @Override
    public void onEnable() {
        var redisUri = RedisURI.Builder.redis("localhost").build();
        var clientResources = ClientResources.builder().ioThreadPoolSize(1).build();
        redisClient = RedisClient.create(clientResources, redisUri);

        // @TODO(traks) when reloading the plugin, this connect can take a very
        // long time (5 seconds). No clue why. Fix it at some point!
        redisConnection = redisClient.connect();
        pubSubConnection = redisClient.connectPubSub();

        // Fetch vanished players when we successfully subscribed or a message
        // is received. Note that we are automatically resubscribed when we lose
        // connection and reconnect (this will call the listener).
        pubSubConnection.addListener(new RedisPubSubAdapter<>() {
            @Override
            public void subscribed(String channel, long count) {
                resetVanishedPlayers();
            }

            @Override
            public void message(String channel, String message) {
                resetVanishedPlayers();
            }
        });

        pubSubConnection.reactive()
                .subscribe(notifyChannel)
                .subscribe();

        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        var resources = redisClient.getResources();
        redisClient.shutdown();

        // we created our own resources in onEnable, so we must shut it down
        // explicitly
        try {
            if (!resources.shutdown().get()) {
                getLogger().log(Level.WARNING, "Failed to shut down Redis resources properly");
            }
        } catch (InterruptedException | ExecutionException e) {
            getLogger().log(Level.WARNING, "Failed to shut down Redis resources properly", e);
        }

        redisClient = null;
        redisConnection = null;
        pubSubConnection = null;
        vanishedPlayers.clear();
    }

    // @NOTE(traks) run before SuperVanish's login listener (runs in MONITOR) so
    // we can alter the vanish state and make SuperVanish think that's what the
    // stored value was
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLogin(PlayerLoginEvent e) {
        var p = e.getPlayer();
        var uuid = p.getUniqueId();
        boolean hidden;

        synchronized (vanishedPlayers) {
            hidden = vanishedPlayers.contains(uuid);
        }

        // @NOTE(traks) This check is actually important, because SuperVanish
        // will insert duplicate entries in its data file if we vanish an
        // already vanished player below
        if (VanishAPI.getPlugin().getVanishStateMgr().isVanished(uuid) == hidden) {
            return;
        }

        // note that this won't create player hide/show events
        VanishAPI.getPlugin().getVanishStateMgr().setVanishedState(
                uuid, null, hidden, null);
    }

    @EventHandler
    public void onPlayerHide(PostPlayerHideEvent e) {
        var p = e.getPlayer();
        var uuid = p.getUniqueId();
        redisConnection.reactive()
                .sadd(vanishedSet, uuid.toString())
                .doOnError(t -> getLogger().log(Level.SEVERE, "Failed to sync hiding " + p.getName(), t))
                .filter(l -> l > 0)
                .then(redisConnection.reactive().publish(notifyChannel, ""))
                .subscribe();
    }

    @EventHandler
    public void onPlayerShow(PostPlayerShowEvent e) {
        var p = e.getPlayer();
        var uuid = p.getUniqueId();
        redisConnection.reactive()
                .srem(vanishedSet, uuid.toString())
                .doOnError(t -> getLogger().log(Level.SEVERE, "Failed to sync showing " + p.getName(), t))
                .filter(l -> l > 0)
                .then(redisConnection.reactive().publish(notifyChannel, ""))
                .subscribe();
    }
}
